/*
 * TrackPiece.h
 *
 *  Created on: Apr 18, 2014
 *      Author: root
 */

#ifndef TRACKPIECE_H_
#define TRACKPIECE_H_

class TrackPiece {
public:
	TrackPiece(const float& length, const bool& hasSwitch = false);
	TrackPiece(const float& angle, const float& radius, const bool& hasSwitch = false);
	~TrackPiece();

	float getAngle() const;
	float getLength() const;
	float getRadius() const;
	bool hasSwitch() const;
	bool isBend() const;

private:
	float length_;
	float angle_;
	float radius_;
	bool hasSwitch_;
	bool bend_;
};

#endif /* TRACKPIECE_H_ */
