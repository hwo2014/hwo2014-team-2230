#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "lapFinished", &game_logic::on_lap_finished },
      { "gameInit", &game_logic::on_game_init },
      { "yourCar", &game_logic::on_your_car }
    }, track_(0)
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{

	const jsoncons::json& ownId = createOwnId();

	jsoncons::json ownInfo;

	for(unsigned int i = 0; i < data.size(); ++i) {
		const jsoncons::json pos = data.at(i);
		if(pos["id"] == ownId) {
			ownInfo = pos["piecePosition"];
			break;
		}
	}

	//TODO control throttle according to ownInfo

    return { make_throttle(0.65) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_lap_finished(const jsoncons::json& data) {
	std::cout << data.to_string() << std::endl;
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data) {

	Track* track = Track::createTrack(data);
	if(track == 0) {
		std::cout << "Error creating track!" << std::endl;
	}
	else {
		track_ = track;
		std::cout << "Track created" << std::endl;
	}
	return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data) {
	std::cout << data.to_string() << std::endl;
	color_ = data["color"].as<std::string>();
	name_ = data["name"].as<std::string>();
	return { make_ping() };
}

jsoncons::json game_logic::createOwnId() const {
	jsoncons::json data;
	data["name"] = name_;
	data["color"] = color_;
	return data;
}
