/*
 * Track.h
 *
 *  Created on: Apr 18, 2014
 *      Author: root
 */

#ifndef TRACK_H_
#define TRACK_H_

#include "trackpiece.h"

#include <string>
#include <vector>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

class Track {
public:

	~Track();
	static Track* createTrack(const jsoncons::json& data);
	bool isNextPieceBend(int currentPieceIndex) const;
	bool isCurrentPieceBend(int currentPieceIndex) const;
	TrackPiece* getPieceAt(int index);

private:
	Track(const std::string& id, const std::string& name, const std::vector<TrackPiece*>& pieces);

	std::string id_;
	std::string name_;
	std::vector<TrackPiece*> pieces_;
};

#endif /* TRACK_H_ */
