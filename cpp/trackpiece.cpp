/*
 * TrackPiece.cpp
 *
 *  Created on: Apr 18, 2014
 *      Author: root
 */

#include "trackpiece.h"

TrackPiece::TrackPiece(const float& length, const bool& hasSwitch) :
	length_(length), hasSwitch_(hasSwitch), angle_(0.0), radius_(0.0),
	bend_(false) {
}

TrackPiece::TrackPiece(const float& angle, const float& radius, const bool& hasSwitch) :
		length_(0.0), hasSwitch_(hasSwitch), angle_(angle), radius_(radius),
			bend_(true){
}


TrackPiece::~TrackPiece() {
}

float TrackPiece::getAngle() const {
	return angle_;
}


float TrackPiece::getLength() const {
	return length_;
}

float TrackPiece::getRadius() const {
	return radius_;
}

bool TrackPiece::hasSwitch() const {
	return hasSwitch_;
}

bool TrackPiece::isBend() const {
	return bend_;
}
