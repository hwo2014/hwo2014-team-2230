/*
 * Track.cpp
 *
 *  Created on: Apr 18, 2014
 *      Author: root
 */

#include "track.h"

#include "trackpiece.h"

#include <vector>

Track* Track::createTrack(const jsoncons::json& data) {

	const jsoncons::json track = data["race"]["track"];
	const std::string name = track["name"].as<std::string>();
	const std::string id = track["id"].as<std::string>();


	std::vector<TrackPiece*> trackPieces;
	for(unsigned int i = 0; i < track["pieces"].size(); ++i) {
		const jsoncons::json piece = track["pieces"].at(i);

		bool hasSwitch = false;

		if(piece.has_member("switch")) {
			hasSwitch = piece["switch"].as_bool();
		}

		if(piece.has_member("length")) {
			float length = (float)piece["length"].as_double();
			trackPieces.push_back(new TrackPiece(length, hasSwitch));
		}
		else if(piece.has_member("radius") && piece.has_member("angle")) {
			float radius = (float)piece["radius"].as_double();
			float angle = (float)piece["angle"].as_double();
			trackPieces.push_back(new TrackPiece(angle, radius, hasSwitch));
		}
		else {
			std::cout << "Invalid piece read from json!";
		}
	}

	return new Track(id, name, trackPieces);

}

Track::Track(const std::string& id, const std::string& name, const std::vector<TrackPiece*>& pieces) :
		id_(id), name_(name), pieces_(pieces) {
}

Track::~Track() {
}

bool Track::isNextPieceBend(int currentPieceIndex) const {
	if(currentPieceIndex < 0 || currentPieceIndex + 1 > pieces_.size() - 1) {
		return false;
	}

	return pieces_.at(currentPieceIndex+1)->isBend();
}

bool Track::isCurrentPieceBend(int currentPieceIndex) const {
	if (currentPieceIndex < 0 || currentPieceIndex > pieces_.size() - 1) {
		return false;
	}

	return pieces_.at(currentPieceIndex)->isBend();
}

TrackPiece* Track::getPieceAt(int index) {
	if(index < 0 || index > pieces_.size() - 1) {
		return 0;
	}
	return pieces_.at(index);
}
